# Desafio Cubos Tecnologia

Nesse projeto foram usados alguns conceitos de uso do cypress que abordam:

- Uso de fixtures;
- Mapeamanto de vários seletores tais como links, textos, tabelas, alerts, propmts, inputs e etc;
- Formulário utilizado como base dos testes dentro do próprio projeto podendo assim ser rodado em qualquer máquina;
- Dado, quando e então sendo usados sem a necessidade de utilização do cucumber.

## Ferramentas

- Cypress na versão 8.5.0
- Node na versão 14.17.0
- Npm na versão 6.14.13

## Documentação e Comandos

- A documentação foi gerada através de um mind map usando o aplicativo XMind, pode ser baixado através desse link [XMind](https://www.xmind.net/m/RP4vid/#)
- Comando para rodar em modo UI via terminal -> npx cypress open
- Comando para rodar em modo Headless via terminal -> npx cypress run
