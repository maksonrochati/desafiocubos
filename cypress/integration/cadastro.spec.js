/// <reference types="cypress" />

/* cadastro.spec.js
 *
 * Como um usuário
 * Quero acessar o formulário
 * Para realizar o cadastro
 */

describe('Formulário campo de treinamento', () => {
  context('Dado que acesso a página do formulário', () => {
    beforeEach(() => {
      const campoTreinamentoPage = '/cypress/support/utils/toTest.html'
      cy.visit(campoTreinamentoPage)
    })
    context('Quando preencho todos os campos e clico em cadastrar', () => {
      beforeEach(() => {
        cy.fixture('userData')
          .as('usuario')
          .then(function () {
            cy.get('#elementosForm\\:nome').type(this.usuario.user.nome)
            cy.get('#elementosForm\\:sobrenome').type(
              this.usuario.user.sobrenome
            )
            cy.get(
              `[name=elementosForm\\:sexo][value=${this.usuario.user.sexo}]`
            ).click()
            cy.get(
              `[name=elementosForm\\:comidaFavorita][value=${this.usuario.user.comida}]`
            ).click()
            cy.get('#elementosForm\\:escolaridade').select(
              this.usuario.user.escolaridade
            )
            cy.get('#elementosForm\\:esportes').select(
              this.usuario.user.esporte
            )
            cy.get('#elementosForm\\:sugestoes').type('Espaço para sugestões')
            cy.get('#elementosForm\\:cadastrar').click()
          })
      })
      it('Então deve mostrar o status como cadastrado e todos os dados inseridos', () => {
        cy.get('#resultado')
          .should('to.be.contain', 'Cadastrado!')
          .and('to.be.contain', 'Makson')
          .and('to.be.contain', 'Rocha')
          .and('to.be.contain', 'Masculino')
          .and('to.be.contain', 'Carne')
          .and('to.be.contain', 'superior')
          .and('to.be.contain', 'Futebol')
          .and('to.be.contain', 'Karate')
        cy.get('#elementosForm\\:sugestoes').should(
          'have.value',
          'Espaço para sugestões'
        )
        cy.get('#elementosForm\\:sexo\\:0').should('be.checked')
        cy.get('#elementosForm\\:comidaFavorita\\:0').should('be.checked')
      })
    })
  })
})
