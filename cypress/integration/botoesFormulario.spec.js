/// <reference types="cypress" />

/* botoesFormulario.spec.js
 *
 * Como um usuário
 * Quero acessar o formulário
 * Para realizar testes ao interagir com botões
 */

describe('Formulário campo de treinamento', () => {
  context('Dado que acesso a página do formulário', () => {
    beforeEach(() => {
      const campoTreinamentoPage = '/cypress/support/utils/toTest.html'
      cy.visit(campoTreinamentoPage)
    })
    context('Quando clico no botão "Clique Me!"', () => {
      beforeEach(() => {
        cy.get('#buttonSimple').click()
      })
      it('Então deve mostrar que o texto do botão foi alterado', () => {
        cy.get('#buttonSimple').should('have.value', 'Obrigado!')
      })
    })
    context('Quando clico no botão "Abrir Popup"', () => {
      beforeEach(() => {
        cy.window().then(function (win) {
          cy.spy(win, 'open').as('winOpen')
          cy.get('#buttonPopUpEasy').click()
        })
      })
      it('Então deve abrir o popup na tela', () => {
        cy.get('@winOpen').should('be.called')
      })
    })
    context('Quando clico no botão "Abrir Popup do mal"', () => {
      beforeEach(() => {
        cy.window().then(function (win) {
          cy.spy(win, 'open').as('winOpen')
          cy.get('#buttonPopUpHard').click()
        })
      })
      it('Então deve abrir o popup na tela', () => {
        cy.get('@winOpen').should('be.called')
      })
    })
    context('Quando clico no botão "Resposta Demorada"', () => {
      beforeEach(() => {
        cy.get('#buttonDelay').click()
      })
      it('Então deve abrir um input no formulario', () => {
        cy.get('#novoCampo').should('exist').type('test2')
      })
    })
    context('Quando verifico o botão "Inativo"', () => {
      it('Então não deve existir no formulario', () => {
        cy.get('#sub-frame-error').should('not.be.exist')
      })
    })
  })
})
