/// <reference types="cypress" />

/* alerts.spec.js
 *
 * Como um usuário
 * Quero acessar o formulário
 * Para realizar testes de alerts
 */

describe('Formulário campo de treinamento', () => {
  context('Dado que acesso a página do formulário', () => {
    beforeEach(() => {
      const campoTreinamentoPage = '/cypress/support/utils/toTest.html'
      cy.visit(campoTreinamentoPage)
    })
    context('Quando clico no botão "Alert"', () => {
      beforeEach(() => {
        cy.get('#alert').click()
      })
      it('Então deve mostrar a mensagem "Alert Simples"', () => {
        cy.on('window:alert', (msg) => {
          expect(msg).to.be.equal('Alert Simples')
        })
      })
    })
    context('Quando clico no botão "Confirm"', () => {
      beforeEach(() => {
        cy.get('#confirm').click()
      })
      it('Então deve abrir o popup na tela', () => {
        cy.on('window:confirm', (msg) => {
          expect(msg).to.be.equal('Confirm Simples')
        })

        cy.on('window:alert', (msg) => {
          expect(msg).to.be.equal('Confirmado')
        })
      })
    })
    context('Quando clico no botão "Prompt"', () => {
      beforeEach(() => {
        cy.window().then((win) => {
          cy.stub(win, 'prompt').returns('42')
        })
        cy.get('#prompt').click()
      })
      it('Então deve abrir o popup na tela', () => {
        cy.on('window:confirm', (msg) => {
          expect(msg).to.be.equal('Era 42?')
        })

        cy.on('window:alert', (msg) => {
          expect(msg).to.be.equal(':D')
        })
      })
    })
  })
})
