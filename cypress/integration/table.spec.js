/// <reference types="cypress" />

/* table.spec.js
 *
 * Como um usuário
 * Quero acessar o formulário
 * Para realizar testes na tabela
 */

describe('Formulário campo de treinamento', () => {
  context('Dado que acesso a página do formulário', () => {
    beforeEach(() => {
      const campoTreinamentoPage = '/cypress/support/utils/toTest.html'
      cy.visit(campoTreinamentoPage)
    })
    context('Quando interagir com a tabela', () => {
      beforeEach(() => {
        cy.get('tr:contains("Maria") > td > input[type=button]').click()
        cy.get('tr:contains("Maria") > td > input[type=checkbox]').check()
        cy.get(':nth-child(2) > > table > tbody > tr > td > input').check()
        cy.get('tr:contains("Maria") > td > input[type=text]').type('Maria')
      })
      it('Então deve validar os dados ', () => {
        cy.on('window:alert', (msg) => {
          expect(msg).to.be.equal('Maria')
        })
        cy.get('tr:contains("Maria") > td > input[type=checkbox]').should(
          'be.checked'
        )
        cy.get(':nth-child(2) > > table > tbody > tr > td > input').should(
          'be.checked'
        )
        cy.get('tr:contains("Maria") > td > input[type=text]').should(
          'have.value',
          'Maria'
        )
      })
    })
  })
})
