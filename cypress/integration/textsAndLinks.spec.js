/// <reference types="cypress" />

/* textsAndLinks.spec.js
 *
 * Como um usuário
 * Quero acessar o formulário
 * Para realizar testes de textos na tela
 */

describe('Formulário campo de treinamento', () => {
  context('Dado que acesso a página do formulário', () => {
    beforeEach(() => {
      const campoTreinamentoPage = '/cypress/support/utils/toTest.html'
      cy.visit(campoTreinamentoPage)
    })
    context('Quando verifico título do formulário', () => {
      it('Então deve conter "Campo de Treinamento"', () => {
        cy.title().should('be.equal', 'Campo de Treinamento')
      })
    })
    context('Quando verifco texto no formulário', () => {
      it('Então deve conter "Cuidado onde clica, muitas armadilhas..."', () => {
        cy.get('.facilAchar').should(
          'have.text',
          'Cuidado onde clica, muitas armadilhas...'
        )
      })
    })
    context('Quando verifico o status', () => {
      it('Então deve conter "Nao cadastrado"', () => {
        cy.get('#resultado').should('have.text', 'Status: Nao cadastrado')
      })
    })
    context('Quando pesquiso pela palavra "Analfabeto"', () => {
      it('Então deve informar que a palavra existe na tela', () => {
        cy.get('td:contains("Analfabeto")').should('be.exist')
      })
    })
    context('Quando clico no link "Voltar"', () => {
      beforeEach(() => {
        cy.get('[href="#').click()
      })
      it('Então deve conter a mensagem "Voltou!"', () => {
        cy.get('#resultado').should('have.text', 'Voltou!')
      })
    })
  })
})
